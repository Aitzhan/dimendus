using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerEnterHandler
{
    static public Transform draggedImage;
    public float width;
    private ReadJson read;
    private WriteJson write;
    void Awake()
    {
        write = FindObjectOfType<WriteJson>();
        read = FindObjectOfType<ReadJson>();
        width = Screen.width / 2;
    }
    public void OnDrag(PointerEventData eventData)
    {
        draggedImage = transform;
        print(draggedImage.name);
        transform.position = Input.mousePosition;
        GetComponent<Image>().raycastTarget = false;

        if (eventData.selectedObject.transform.position.x < width)
            eventData.selectedObject.transform.parent.SetParent(read.left.transform);
        else
            eventData.selectedObject.transform.parent.SetParent(read.right.transform);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = Vector3.zero;
        draggedImage = null;
        GetComponent<Image>().raycastTarget = true;
        write.Save();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (draggedImage != null)
        {
            Transform lastParent = transform.parent;
            transform.parent = draggedImage.parent;
            draggedImage.parent = lastParent;
            draggedImage.transform.localPosition = Vector3.zero;
            transform.localPosition = Vector3.zero;
            draggedImage = null;
        }
    }
}