﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LitJson;
using UnityEngine.UI;

public class ReadJson : MonoBehaviour
{
    private string jsonString;
    private JsonData itemData;

    public GameObject inputPrefab;

    public Text titleLeft;
    public Text titleRight;

    public GameObject left;
    public GameObject right;

    public List<InputField> leftInputs;
    public List<InputField> rightInputs;

    private WriteJson write;
    void Start()
    {
        write = FindObjectOfType<WriteJson>();
        jsonString = File.ReadAllText(Application.streamingAssetsPath + "/Data.json");
        itemData = JsonMapper.ToObject(jsonString);
        left.GetComponent<GridLayoutGroup>().enabled=false;
        right.GetComponent<GridLayoutGroup>().enabled = false;
        Read(); 
    }
  
   public void Read()
    {
        if (itemData[0]["item"].Count > 0)
        {           
            for (int i = 0; i < itemData[0]["item"].Count; i++)
            {
                GameObject inputLeft = Instantiate(inputPrefab, transform.position, Quaternion.identity);
                inputLeft.gameObject.transform.SetParent(left.transform);
                inputLeft.transform.GetChild(0).GetComponent<InputField>().text = itemData[0]["item"][i].ToString();
                leftInputs.Add(inputLeft.transform.GetChild(0).GetComponent<InputField>());
            }
        }
        if (itemData[1]["item"].Count > 0)
        {
            for (int i = 0; i < itemData[1]["item"].Count; i++)
            {
                GameObject inputRight = Instantiate(inputPrefab, transform.position, Quaternion.identity);
                inputRight.gameObject.transform.SetParent(right.transform);
                inputRight.transform.GetChild(0).GetComponent<InputField>().text = itemData[1]["item"][i].ToString();
                rightInputs.Add(inputRight.transform.GetChild(0).GetComponent<InputField>());
            }
           
        }
        left.GetComponent<GridLayoutGroup>().enabled = true;
        right.GetComponent<GridLayoutGroup>().enabled = true;
        titleLeft.text = itemData[0]["title"].ToString() + ": amount of elements " + left.transform.childCount;
        titleRight.text = itemData[1]["title"].ToString() + ": amount of elements " + right.transform.childCount;
    }
    public void UpdateUI()
    {
    
        if (leftInputs.Count > 0)
        {
            for (int i = 0; i < leftInputs.Count; i++)
            {
                Destroy(leftInputs[i].gameObject.transform.parent.gameObject);
            }
            leftInputs.Clear();
        }
        if (rightInputs.Count > 0)
        {
            for (int i = 0; i < rightInputs.Count; i++)
            {
                Destroy(rightInputs[i].gameObject.transform.parent.gameObject);
            }
            rightInputs.Clear();
        }
        if (write.GetData()[0].item.Count>0)
        {
            for (int i = 0; i < write.GetData()[0].item.Count; i++)
            {
                GameObject inputLeft = Instantiate(inputPrefab, transform.position, Quaternion.identity);
                inputLeft.gameObject.transform.SetParent(left.transform);
                inputLeft.transform.GetChild(0).GetComponent<InputField>().text = write.GetData()[0].item[i];
                leftInputs.Add(inputLeft.transform.GetChild(0).GetComponent<InputField>());
            }
        }
        if (write.GetData()[1].item.Count > 0)
        {
            for(int i = 0; i < write.GetData()[1].item.Count; i++)
            {
                GameObject inputRight = Instantiate(inputPrefab, transform.position, Quaternion.identity);
                inputRight.gameObject.transform.SetParent(right.transform);
                inputRight.transform.GetChild(0).GetComponent<InputField>().text = write.GetData()[1].item[i];
                rightInputs.Add(inputRight.transform.GetChild(0).GetComponent<InputField>());
            }
        }
        left.GetComponent<GridLayoutGroup>().enabled = true;
        right.GetComponent<GridLayoutGroup>().enabled = true;
        titleLeft.text = itemData[0]["title"].ToString() + ": amount of elements " + write.GetData()[0].item.Count;
        titleRight.text = itemData[1]["title"].ToString() + ": amount of elements " + write.GetData()[1].item.Count;
    }

}
