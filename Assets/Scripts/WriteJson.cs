﻿using UnityEngine;
using LitJson;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[Serializable]
public class Data
{
    public string title;
    public List<string> item;

    public Data()
    {
        item = new List<string>();
    }
   
}
public class WriteJson : MonoBehaviour
{
    [SerializeField]
    private List<Data> data;
    private JsonData dataJason;
    private ReadJson read;
    // Start is called before the first frame update
    public List<Data> GetData()
    {
         return data;
    }
    void Start()
    {
        read = FindObjectOfType<ReadJson>();
        data = new List<Data>();
        data.Add(new Data());
        data.Add(new Data());
     
    }
    public void Save()
    {
        data[0].item.Clear();
        data[1].item.Clear();
        
        data[0].title = "Left";
        data[1].title = "Right";
        for (int i = 0; i < read.left.transform.childCount; i++)
        {
            data[0].item.Add(read.left.transform.GetChild(i).transform.GetChild(0).gameObject.GetComponent<InputField>().text);
        }
        for (int i = 0; i < read.right.transform.childCount; i++)
        {
            data[1].item.Add(read.right.transform.GetChild(i).transform.GetChild(0).gameObject.GetComponent<InputField>().text);
        }
        dataJason = JsonMapper.ToJson(data);
        File.WriteAllText(Application.streamingAssetsPath + "/Data.json", dataJason.ToString());
      
        read.left.GetComponent<GridLayoutGroup>().enabled = false;
        read.right.GetComponent<GridLayoutGroup>().enabled = false;

        read.UpdateUI();
    }
    private void OnApplicationQuit()
    {
        Save();
    }
}
